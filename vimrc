set nocompatible               " be iMproved (not the plain vi)
filetype off                   " required!
set hidden                     " No annoying question to save buffers first.
set noignorecase               " Case does matter

nnoremap ; :

" Do not put swap and backup files next to sourcefiles
" angular-cli watcher triggers on them.
" The // adds full path to make files unique
set directory=~/.vim/swapfiles//
set backupdir=~/.vim/backupfiles//

" Add to the runtime path
set rtp+=~/.vim/bundle/neobundle.vim
call neobundle#begin(expand('~/.vim/bundle/'))
NeoBundleFetch 'Shougo/neobundle.vim'

" matchit, % match of xml/html
NeoBundle 'matchit.zip'

" Autocomplete brackets, quotes etc.
NeoBundle 'Raimondi/delimitMate' "{{{
let delimitMate_expand_cr=1
autocmd FileType markdown,vim let b:loaded_delimitMate=1

NeoBundle 'skwp/vim-easymotion' "{{{
" NeoBundle 'Lokaltog/vim-easymotion'
let g:EasyMotion_keys = 'asdfghjklqwertyuiopzxcvbnm'
"autocmd ColorScheme * highlight EasyMotionTarget ctermfg=32 guifg=#0087df
"autocmd ColorScheme * highlight EasyMotionShade ctermfg=237 guifg=#3a3a3a
"}}}

" autocompletion
" NeoBundle 'Valloric/YouCompleteMe' "{{{
" let g:ycm_complete_in_comments_and_strings=1
" let g:ycm_key_list_select_completion=['<C-n>', '<Down>']
" let g:ycm_key_list_previous_completion=['<C-p>', '<Up>']
" let g:ycm_filetype_blacklist={'unite': 1}
"}}}
" NeoBundle 'SirVer/ultisnips' "{{{
" let g:UltiSnipsExpandTrigger="<tab>"
" let g:UltiSnipsJumpForwardTrigger="<tab>"
" let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
" let g:UltiSnipsSnippetsDir='~/.vim/snippets'
"}}}

NeoBundle 'Shougo/neocomplete.vim'

"Note: This option must set it in .vimrc(_vimrc).  NOT IN .gvimrc(_gvimrc)!
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use neocomplete.
let g:neocomplete#enable_at_startup = 1
" Use smartcase.
let g:neocomplete#enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 3
let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'

" Define dictionary.
let g:neocomplete#sources#dictionary#dictionaries = {
    \ 'default' : '',
    \ 'vimshell' : $HOME.'/.vimshell_hist',
    \ 'scheme' : $HOME.'/.gosh_completions'
        \ }

" Define keyword.
if !exists('g:neocomplete#keyword_patterns')
    let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'

" Plugin key-mappings.
inoremap <expr><C-g>     neocomplete#undo_completion()
inoremap <expr><C-l>     neocomplete#complete_common_string()

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return neocomplete#close_popup() . "\<CR>"
  " For no inserting <CR> key.
  "return pumvisible() ? neocomplete#close_popup() : "\<CR>"
endfunction
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><C-y>  neocomplete#close_popup()
inoremap <expr><C-e>  neocomplete#cancel_popup()
" Close popup by <Space>.
"inoremap <expr><Space> pumvisible() ? neocomplete#close_popup() : "\<Space>"

" For cursor moving in insert mode(Not recommended)
"inoremap <expr><Left>  neocomplete#close_popup() . "\<Left>"
"inoremap <expr><Right> neocomplete#close_popup() . "\<Right>"
"inoremap <expr><Up>    neocomplete#close_popup() . "\<Up>"
"inoremap <expr><Down>  neocomplete#close_popup() . "\<Down>"
" Or set this.
"let g:neocomplete#enable_cursor_hold_i = 1
" Or set this.
"let g:neocomplete#enable_insert_char_pre = 1

" AutoComplPop like behavior.
"let g:neocomplete#enable_auto_select = 1

" Shell like behavior(not recommended).
"set completeopt+=longest
"let g:neocomplete#enable_auto_select = 1
"let g:neocomplete#disable_auto_complete = 1
"inoremap <expr><TAB>  pumvisible() ? "\<Down>" : "\<C-x>\<C-u>"

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

" Enable heavy omni completion.
if !exists('g:neocomplete#sources#omni#input_patterns')
  let g:neocomplete#sources#omni#input_patterns = {}
endif
"let g:neocomplete#sources#omni#input_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
"let g:neocomplete#sources#omni#input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
"let g:neocomplete#sources#omni#input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'

" For perlomni.vim setting.
" https://github.com/c9s/perlomni.vim
let g:neocomplete#sources#omni#input_patterns.perl = '\h\w*->\h\w*\|\h\w*::'


let g:flake8_ignore="E126,E127,E128,E501"
NeoBundle 'vim-flake8'
autocmd BufWritePost *.py call Flake8()
" Ignore indent errors
NeoBundle 'bufexplorer.zip'
" snippets for ultisnip
" NeoBundle 'honza/vim-snippets'

NeoBundle 'tpope/vim-unimpaired' " for ]q and [q
NeoBundle 'fugitive.vim'
" NeoBundle 'YankRing.vim'

" File finder
NeoBundle 'kien/ctrlp.vim', { 'depends': 'tacahiroy/ctrlp-funky' }
let g:ctrlp_clear_cache_on_exit=1
let g:ctrlp_max_height=40
let g:ctrlp_show_hidden=0
let g:ctrlp_follow_symlinks=1
let g:ctrlp_working_path_mode='r'
let g:ctrlp_max_files=20000
let g:ctrlp_cache_dir='~/.vim/.cache/ctrlp'
let g:ctrlp_extensions=['funky']

" Coffeescript support
NeoBundle 'kchmck/vim-coffee-script'
let coffee_make_options = '--map'
autocmd BufWritePost *.coffee silent make
autocmd BufNewFile,BufReadPost *.coffee setl shiftwidth=2 expandtab
autocmd QuickFixCmdPost [^l]* nested cwindow
autocmd QuickFixCmdPost    l* nested lwindow

" Typescript support
NeoBundle 'leafgarland/typescript-vim'
" autocmd BufWritePost *.ts silent make
let g:typescript_compiler_options = ''
let g:typescript_compiler_target = ''
autocmd BufNewFile,BufReadPost *.ts setl shiftwidth=2 expandtab

" SQL Utilities
NeoBundle 'Align'
NeoBundle 'SQLUtilities'

" html, yaml and templates
autocmd BufReadPost,BufNewFile *.html,*.pt,*.yml setl sw=2 expandtab


nmap \ [ctrlp]
nnoremap [ctrlp] <nop>

nnoremap [ctrlp]t :CtrlPBufTag<cr>
nnoremap [ctrlp]T :CtrlPTag<cr>
nnoremap [ctrlp]l :CtrlPLine<cr>
nnoremap [ctrlp]o :CtrlPFunky<cr>
nnoremap [ctrlp]b :CtrlPBuffer<cr>


" NeoBundleLazy 'klen/python-mode', {'autoload':{'filetypes':['python']}} "{{{
" let g:pymode_rope=0
" let g:pymode_lint_ignore="E126,E127,E128"
" "}}}
" NeoBundleLazy 'davidhalter/jedi-vim', {'autoload':{'filetypes':['python']}} "{{{
" let g:jedi#popup_on_dot=0
" "}}}


" Shortcut for the explorer
map <F3> :BufExplorer<CR>
" Use TAB to cycle through windows
map <Tab> <C-W>W

" Easy window navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" clear search term
nmap <silent> ,/ :nohlsearch<CR>

" A better mapleader
let mapleader="\<Space>"
let g:mapleader="\<Space>"

" Better copy/paste to system clipboard
vmap <Leader>y "+y
vmap <Leader>d "+d
nmap <Leader>p "+p
nmap <Leader>P "+P
vmap <Leader>p "+p
vmap <Leader>P "+P

" Hide some files in the explorer
let g:netrw_list_hide="\.swp$,\.pyc$"

" Tab-completion on the vim cmdline
:set wildmode=longest,list
" Always have some space around cursor
:set scrolloff=2


" set ignorecase "ignore case for searching
" set smartcase "do case-sensitive if there's a capital letter
if executable('ack')
set grepprg=ack\ --nogroup\ --column\ --smart-case\ --nocolor\ --follow\ $*
set grepformat=%f:%l:%c:%m
endif
"if executable('ag')
"set grepprg=ag\ --nogroup\ --column\ --smart-case\ --nocolor\ --follow
"set grepformat=%f:%l:%c:%m
"endif

" Remove trailing whitespace
autocmd BufWritePre *.py :%s/\s\+$//e


" Always find the tags file
set tags=TAGS;/,tags;/

" Use nice font on ubuntu
set guifont=Ubuntu\ Mono\ 14

" Load my colorscheme
NeoBundle 'gregsexton/Atom'

" Note-taking
" NeoBundle 'xolox/vim-misc'
" NeoBundle 'xolox/vim-notes'
" let g:notes_directories = ['~/data/Notes']
" let g:notes_suffix = '.txt'
" let g:notes_smart_quotes = 0

" silver searcher
NeoBundle 'rking/ag.vim'

" Abolish
NeoBundle 'tpope/vim-abolish'

" required
call neobundle#end()

" Set atom colorsheet
:colo atom

" Some handy abbreviations

abb pdb import pdb; pdb.set_trace()
abb ipdb import ipdb; ipdb.set_trace()
abb ptb import pytest; pytest.set_trace()
abb tmpout open('/tmp/out.html', 'w').write(res.content)

" Switch on filetype detection and indentation
filetype plugin indent on     " required! 
syntax on
set shiftwidth=4
set smarttab
set tabstop=4
set expandtab
set textwidth=0
set hls
set is
set guioptions-=T
set guioptions-=l
set guioptions-=L
set guioptions-=r
set guioptions-=R
set guioptions-=b
set autochdir
set mouse=c  " mouse to command-line mode, get rid of annoying clicks on touchpad.

" Ctl-arrow for window switching
" nmap <silent> <C-Left> :wincmd h<CR>
" nmap <silent> <C-Right> :wincmd l<CR>
" nmap <silent> <C-Up> :wincmd k<CR>
" nmap <silent> <C-Down> :wincmd j<CR>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

nmap =j :%!python -m json.tool<CR>
